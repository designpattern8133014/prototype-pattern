package prototype;

public class Test1 {

    public static void main(String[] args) {
        Statement statement = Statement.getStatementBuilder().projection("select firstname, lastname")
                .from("from employee")
                .where("where id = 12")
                .createStatement();

        /*System.out.println(statement.hashCode());
        Statement clone = statement.clone();
        System.out.println(clone.hashCode());// we have different hashCode
*/

        statement.setRecord(new Record());
        System.out.println(statement.hashCode());
        System.out.println(statement.getRecord().hashCode());
        Statement clone1 = statement.clone();
        System.out.println(clone1.hashCode());// we have different hashCode
        System.out.println(statement.getRecord().hashCode()); // we have a same hashcode
    }
}
