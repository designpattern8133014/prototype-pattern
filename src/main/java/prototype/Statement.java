package prototype;

public class Statement implements Cloneable{

    private String projection;
    private String where;
    private String from;
    private String groupBy;
    private String orderBy;
    private Record record;

    public Statement(String projection, String where, String from, String groupBy, String orderBy, Record record) {
        this.projection = projection;
        this.where = where;
        this.from = from;
        this.groupBy = groupBy;
        this.orderBy = orderBy;
        this.record = record;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public String getProjection() {
        return projection;
    }

    public void setProjection(String projection) {
        this.projection = projection;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public Statement clone() {
        try {
            return (Statement) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public static StatementBuilder getStatementBuilder(){
        return new StatementBuilder();
    }

    public static class StatementBuilder{
        private String projection;
        private String where;
        private String from;
        private String groupBy;
        private String orderBy;
        private Record record;

        public StatementBuilder projection(String projection) {
            this.projection = projection;
            return this;
        }

        public StatementBuilder where(String where) {
            this.where = where;
            return this;
        }

        public StatementBuilder from(String from) {
            this.from = from;
            return this;
        }

        public StatementBuilder groupBy(String groupBy) {
            this.groupBy = groupBy;
            return this;
        }

        public StatementBuilder orderBy(String orderBy) {
            this.orderBy = orderBy;
            return this;
        }

        public StatementBuilder record(Record record) {
            this.record = record;
            return this;
        }

        public Statement createStatement(){
            return new Statement(projection,where,from,groupBy,orderBy,record);
        }
    }
}
